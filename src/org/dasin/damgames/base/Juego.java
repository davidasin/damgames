package org.dasin.damgames.base;

import java.util.Date;

/**
 * Created by DAM on 23/10/14.
 */
public class Juego {

    private String nombre;
    private String genero;
    private Date fechaSalida;
    private String plataforma;
    private float precio;
    private int edadMinima;

    private Programador programador;
    private Compania compania;

    public String getNombre() {
        return nombre;
    }

    public String getGenero() {
        return genero;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public float getPrecio() {
        return precio;
    }

    public int getEdadMinima() {
        return edadMinima;
    }

    public Programador getProgramador() {
        return programador;
    }

    public Compania getCompania() {
        return compania;
    }

    public void setNombre(String nombre) {
        nombre = nombre;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }

    public void setProgramador(Programador programador) {
        this.programador = programador;
    }

    public void setCompania(Compania compania) {
        this.compania = compania;
    }

    public String toString(){
        return nombre + " ("+plataforma+")";
    }
}

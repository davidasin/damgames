package org.dasin.damgames.base;

import java.util.Date;
import java.util.List;

/**
 * Created by DAM on 23/10/14.
 */
public class Compania {

    private String nombre;
    private String domicilio;
    private String pais;
    private Date fechaFundacion;

    private List<Programador> listaProgramadores;
    private List<Juego> listaJuegos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getFechaFundacion() {
        return fechaFundacion;
    }

    public void setFechaFundacion(Date fechaFundacion) {
        this.fechaFundacion = fechaFundacion;
    }

    public List<Programador> getListaProgramadores() {
        return listaProgramadores;
    }

    public void setListaProgramadores(List<Programador> listaProgramadores) {
        this.listaProgramadores = listaProgramadores;
    }

    public List<Juego> getListaJuegos() {
        return listaJuegos;
    }

    public void setListaJuegos(List<Juego> listaJuegos) {
        this.listaJuegos = listaJuegos;
    }

    public String toString(){
        return nombre;
    }
}

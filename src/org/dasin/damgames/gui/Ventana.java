package org.dasin.damgames.gui;

import com.toedter.calendar.JDateChooser;
import org.dasin.damgames.base.Compania;
import org.dasin.damgames.base.Juego;
import org.dasin.damgames.base.Programador;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAM on 23/10/14.
 */
public class Ventana {

    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField tfnombreJue;
    private JTextField tfgenero;
    private JTextField tfplataforma;
    private JTextField tfprecio;
    private JComboBox cbprogramador;
    private JComboBox cbcompania1;
    private JTextField tfbusquedaJue;
    private JButton btnuevoJ;
    private JButton btguardarJ;
    private JButton btmodificarJ;
    private JButton bteliminarJ;
    private JButton btcancelarJ;
    private JButton btprimeroJ;
    private JButton btanteriorJ;
    private JButton btsiguienteJ;
    private JButton btultimoJ;
    private JList lprogramadores;
    private JList ljuegos;
    private JList lcompanias;
    private JTextField tfnombrePro;
    private JTextField tfapellidos;
    private JTextField tfemail;
    private JTextField tfsalario;
    private JComboBox cbcompania2;
    private JTextField tfnombreCompania;
    private JTextField tfdomicilio;
    private JTextField tfpais;
    private JTextField tfbusquedaComp;
    private JTextField tfbusquedaProg;
    private JButton btnuevoP;
    private JButton btguardarP;
    private JButton btmodificarP;
    private JButton bteliminarP;
    private JButton btcancelarP;
    private JButton btprimeroP;
    private JButton btanteriorP;
    private JButton btsiguienteP;
    private JButton btultimoP;
    private JButton btnuevoC;
    private JButton btguardarC;
    private JButton btmodificarC;
    private JButton bteliminarC;
    private JButton btcancelarC;
    private JButton btprimeroC;
    private JButton btanteriorC;
    private JButton btsiguienteC;
    private JButton btultimoC;
    private JList ljuegosC;
    private JList lprogramadoresC;
    private JList ljuegosP;
    private JTextField tfedadMinima;
    private JDateChooser dcfechaJ;
    private JDateChooser dcfechaC;

    private List<Juego> listaJuegos;
    private List<Programador> listaProgramadores;
    private List<Compania> listaCompanias;
    private DefaultListModel<Juego> modeloListaJuegos;
    private DefaultListModel<Programador> modeloListaProgramadores;
   private DefaultListModel<Compania> modelolistaCompania;

    private int posicionJuegos;
    private int posicionProgramadores;
    private int posicionCompanias;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public Ventana(){
        listaJuegos = new ArrayList<Juego>();
        listaProgramadores = new ArrayList<Programador>();
        listaCompanias = new ArrayList<Compania>();

        modeloListaJuegos = new DefaultListModel<Juego>();
        ljuegos.setModel(modeloListaJuegos);
        modeloListaProgramadores = new DefaultListModel<Programador>();
        lprogramadores.setModel(modeloListaProgramadores);
        modelolistaCompania = new DefaultListModel<Compania>();
        lcompanias.setModel(modelolistaCompania);

        posicionJuegos = 0;
        posicionProgramadores = 0;
        posicionCompanias = 0;

        dcfechaJ.setEnabled(false);
        dcfechaC.setEnabled(false);

        tabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                cargarPestana();
            }
        });

        //listenersJUEGO
        btnuevoJ.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                modoNuevoJuego();
            }
        });

        //listenersPROGRAMADOR
        btnuevoP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                modoNuevoProgramador();
            }
        });

        //listenersCOMPAÑIA
        btnuevoC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                modoNuevaCompania();
            }
        });

    }

    public void cargarPestana(){
        int p = tabbedPane1.getSelectedIndex();
        switch (p){
            case 1:
                cargarPestanaJuegos();
                break;
            case 2:
                //cargarPestanaProgramadores();
                break;
            case 3:
                //cargarPestanaCompanias();
                break;
        }
    }

    //METODOS
    //Juego
    public void cargarPestanaJuegos(){
        refrescarComboProgramadores();
        refrescarComboCompanias();

        Juego juego = listaJuegos.get(posicionJuegos);

        tfnombreJue.setText(juego.getNombre());
        tfgenero.setText(juego.getGenero());
        dcfechaJ.setDate(juego.getFechaSalida());
        tfplataforma.setText(juego.getPlataforma());
        tfprecio.setText(String.valueOf(juego.getPrecio()));
        tfedadMinima.setText(String.valueOf(juego.getEdadMinima()));
        cbprogramador.setSelectedItem(juego.getProgramador());
        cbcompania1.setSelectedItem(juego.getCompania());
    }

    public void refrescarComboProgramadores(){
        cbprogramador.removeAllItems();
        for (Programador programador: listaProgramadores){
            cbprogramador.addItem(programador);
        }
    }

    public void refrescarComboCompanias(){
        cbcompania1.removeAllItems();
        for (Compania compania: listaCompanias){
            cbcompania1.addItem(compania);
        }
    }

    public void refrescarListaJuegos(){
        modeloListaJuegos.remo
    }

    //Programador

    //Compañia



    //MODOS DE VISUALIZACION
    //Juego
    public void modoNuevoJuego(){
        //TF
        tfnombreJue.setEditable(true);
        tfgenero.setEditable(true);
        tfedadMinima.setEditable(true);
        tfplataforma.setEditable(true);
        tfprecio.setEditable(true);
        tfbusquedaJue.setEditable(false);
        //CB
        cbprogramador.setEnabled(true);
        cbcompania1.setEnabled(true);
        //BT
        btnuevoJ.setEnabled(false);
        btguardarJ.setEnabled(true);
        btmodificarJ.setEnabled(false);
        bteliminarJ.setEnabled(false);
        btcancelarJ.setEnabled(true);

        btprimeroJ.setEnabled(false);
        btanteriorJ.setEnabled(false);
        btsiguienteJ.setEnabled(false);
        btultimoJ.setEnabled(false);
        //L
        ljuegos.setEnabled(false);
        //DC
        dcfechaJ.setEnabled(true);
    }

    //Programador
    public void modoNuevoProgramador(){
        //TF
        tfnombrePro.setEditable(true);
        tfapellidos.setEditable(true);
        tfemail.setEditable(true);
        tfsalario.setEditable(true);
        tfbusquedaProg.setEditable(false);
        //CB
        cbcompania2.setEnabled(true);
        //BT
        btnuevoP.setEnabled(false);
        btguardarP.setEnabled(true);
        btmodificarP.setEnabled(false);
        bteliminarP.setEnabled(false);
        btcancelarP.setEnabled(true);

        btprimeroP.setEnabled(false);
        btanteriorP.setEnabled(false);
        btsiguienteP.setEnabled(false);
        btultimoP.setEnabled(false);
        //L
        lprogramadores.setEnabled(false);

    }

    //Compañia
    public void modoNuevaCompania(){
        //TF
        tfnombreCompania.setEditable(true);
        tfdomicilio.setEditable(true);
        tfpais.setEditable(true);
        tfbusquedaComp.setEditable(false);
        //BT
        btnuevoC.setEnabled(false);
        btguardarC.setEnabled(true);
        btmodificarC.setEnabled(false);
        bteliminarC.setEnabled(false);
        btcancelarC.setEnabled(true);

        btprimeroC.setEnabled(false);
        btanteriorC.setEnabled(false);
        btsiguienteC.setEnabled(false);
        btultimoC.setEnabled(false);
        //DC
        dcfechaC.setEnabled(true);
        //L
        lcompanias.setEnabled(false);
    }

    //NAVEGACION
}
